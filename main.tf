module "sg_base" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.17.0"
  for_each = local.security_groups_base

  name        = each.value.name
  description = each.value.description
  vpc_id      = var.vpc_id


  ingress_with_cidr_blocks = each.value.ingress_with_cidr_blocks
  egress_rules             = each.value.egress_rules

  ingress_with_self = each.value.ingress_with_self
  tags                    = var.tags

}


module "sg_computed" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.17.0"
  for_each = local.security_groups_computed

  name        = each.value.name
  description = each.value.description
  vpc_id      = var.vpc_id



  computed_ingress_with_source_security_group_id = [
    for value in each.value.computed_ingress_with_source_security_group_id : merge(
      { for key, val in value : key => val if key != "source_security_group_id" },
      { for key, val in value : key => module.sg_base["${val} base"].this_security_group_id if key == "source_security_group_id" }
  )]


  number_of_computed_ingress_with_source_security_group_id = length(each.value.computed_ingress_with_source_security_group_id)

  egress_rules = each.value.egress_rules

  ingress_with_self = each.value.ingress_with_self
  tags                    = var.tags

}
output base_security_group_ids {
  value       = module.sg_base.*
  description = "Output de los security groups bases"
}


output computed_security_group_ids {
  value       = module.sg_computed.*
  description = "Output de los security groups computados"
}

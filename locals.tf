locals {
    security_groups_base = { for sg in var.security_groups_base : sg.name => sg }
    security_groups_computed = { for sg in var.security_groups_computed : sg.name => sg }
}
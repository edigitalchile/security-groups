# REQUIRED PARAMETERS

variable vpc_id {
  type        = string
  description = "Id de la vpc a las cuales perteneceran los security groups"
}

# OPTIONAL PARAMETERS

variable security_groups_base {

  type = list(object({
    name                     = string
    description              = string
    ingress_with_cidr_blocks = list(map(string))
    ingress_with_self        = list(map(string))
    egress_rules             = list(string)
  }))
  description = "Configuracion de los security groups que no dependen de otros"
  default = []
}

variable security_groups_computed {

  type = list(object({
    name                                           = string
    description                                    = string
    computed_ingress_with_source_security_group_id = list(map(string))
    ingress_with_self                              = list(map(string))
    egress_rules                                   = list(string)
  }))
  description = "Configuracion de los security groups que dependen de los security groups base"
  default = []
}

variable tags {
  type        = map(string)
  description = "Mapa de tags para asignar a los security groups"
  default     = null
}








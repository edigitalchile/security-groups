# Modulo de terraform para security groups en aws

Este modulo estaba basado en el modulo oficial de terraform para la creacion de security groups: https://registry.terraform.io/modules/terraform-aws-modules/security-group/aws/3.17.0 


### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| vpc_id  | Id de la vpc a las cuales perteneceran los security groups  | SI  | N/A  |
| security_groups_base  | Configuracion de los security groups que no dependen de otros  | NO  | []  |
| security_groups_computed  | Configuracion de los security groups que dependen de los security groups base  | NO  | []  |
| tags  | Mapa de tags para asignar a los security groups  | NO  | null  |


### Outputs

| Nombre  | Descripción  |
|---|---|
| base_security_group_ids  | Array que contiene la informacion de los security group base creados. Los atributos son lo que aparecen en la documentacion del modulo oficial de security groups.   |
| computed_security_group_ids  | Array que contiene la informacion de los security group computed creados. Los atributos son lo que aparecen en la documentacion del modulo oficial de security groups.  |